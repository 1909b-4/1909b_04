import { useEffect } from 'react';
import { BrowserRouter } from 'react-router-dom';
import RouteView from './router/RouteView';
import router from './router/routerConfig';
// Mobx
import { Provider } from 'mobx-react';
import Mobstore from './store/MobxIndex';
// redux
import { IntlProvider } from 'react-intl';
import { inject, observer } from 'mobx-react';
// @inject('titleStore')
// @observer
const App = observer((props) => {
  useEffect(() => {
    console.log(Mobstore.titleStore.lange, '123456');
  }, []);
  return (
    <div>
      <Provider {...Mobstore}>
        <IntlProvider
          messages={Mobstore.titleStore.message}
          locale={Mobstore.titleStore.lange}
          defaultLocale="en"
        >
          <BrowserRouter>
            <RouteView router={router}></RouteView>
          </BrowserRouter>
        </IntlProvider>
      </Provider>
    </div>
  );
});
export default inject((Mobstore) => Mobstore)(App);
// export default inject('titleStore')(observer(App));
