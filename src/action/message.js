import api from '../api/index';

export let mess_list = () => {
  return (dispatch) => {
    api.BoardList().then((res) => {
      dispatch({
        type: 'MESS_LIST',
        payload: res,
      });
    });
  };
};

export let liuyan = (shuru) => {
  return {
    type: 'LIUYAN',
    payload: shuru,
  };
};
export let yi_huifu = (shuru) => {
  return {
    type: 'YI_HUIFU',
    payload: shuru,
  };
};

export let er_huifu = (item) => {
  return {
    type: 'ER_HUIFU',
    payload: item
  }
}

export let tiao_shu = (page) => {
  return {
    type: 'TIAO_SHU',
    payload: page
  }
}
