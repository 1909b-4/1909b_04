import httpTool from '../utils/httpTool';
import { message } from 'antd';
const context = require.context('../api/modules', false, /\.js$/); //require.context()返回值是一个函数
//合并对象
const dataObj = context.keys().reduce((prev, cur) => {
  let def = { ...prev, ...context(cur).default };
  return def;
}, {});

const httpAxios = httpTool({
  timeout: 1,
  commonHeaders: {
    token: localStorage.getItem('token') || '1909',
  },
  failMessage: (msg) => {
    message.warn(msg);
  },
});

const api = Object.keys(dataObj).reduce((prev, cur) => {
  prev[cur] = (data) => {
    return httpAxios({
      ...dataObj[cur],
      [dataObj[cur].method === 'get' ? 'params' : 'data']: data,
    });
  };
  return prev;
}, {});

export default api;
