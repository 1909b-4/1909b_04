import axios from 'axios';

export let MessList = () => {
  return axios.get('/api/article');
};

export let BoardList = () => {
  return axios.get(
    '/api/comment/host/10257f2f-3367-4183-b496-27baf0f3743e?page=1&pageSize=6'
  );
};
