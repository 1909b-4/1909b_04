const File = {
  getArticle: {
    method: 'get',
    url: '/api/article/archives',
  },
};

export default File;
