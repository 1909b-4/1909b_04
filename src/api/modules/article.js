const article = {
  getRecommend: {
    method: 'get',
    url: '/api/article/recommend',
  },
  getArticle: {
    method: 'get',
    url: '/api/article',
  },
};

export default article;
