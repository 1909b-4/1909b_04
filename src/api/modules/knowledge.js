const knowledge = {
  getKnowledge: {
    method: 'get',
    url: '/api/Knowledge',
  },
};

export default knowledge;
