const message = {
  MessList: {
    method: 'get',
    url: '/api/article',
  },
  BoardList: {
    method: 'get',
    url: '/api/comment/host/10257f2f-3367-4183-b496-27baf0f3743e?page=1&pageSize=6',
  },
};

export default message;
