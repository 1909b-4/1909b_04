import React, { Component } from 'react';
import labellist from '../page/File/Articlelabel';
import { NavLink } from 'react-router-dom';

class ArticleLabel extends Component {
  state = {
    label: labellist,
  };
  render() {
    const { label } = this.state;
    return (
      <div>
        {label && label.length
          ? label.map((item, index) => {
              return (
                <NavLink key={index} to={item.router}>
                  {item.leading}
                </NavLink>
              );
            })
          : null}
      </div>
    );
  }
}

export default ArticleLabel;
