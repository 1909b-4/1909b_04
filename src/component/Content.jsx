import React, { Component } from 'react';
import api from '../api/index';
import { withRouter } from 'react-router-dom';

export class Content extends Component {
  state = {
    articlelist: [],
  };
  componentDidMount() {
    api.getArticle().then((res) => {
      this.setState({
        articlelist: res.data[0],
      });
    });
  }
  toacticle(item) {
    console.log(this);
    // 不好使
    this.props.history.push('/Index/article', item);
  }
  render() {
    let { articlelist } = this.state;
    function utc2timestamp(utc_datetime) {
      var T_pos = utc_datetime.indexOf('T');
      var Z_pos = utc_datetime.indexOf('Z');
      var year_month_day = utc_datetime.substr(0, T_pos);
      var hour_minute_second = utc_datetime.substr(
        T_pos + 1,
        Z_pos - T_pos - 1
      );
      var new_datetime = year_month_day + ' ' + hour_minute_second; // 2017-03-31 08:02:06
      var timestamp;
      timestamp = new Date(Date.parse(new_datetime));
      timestamp = timestamp.getTime();
      timestamp = timestamp + 8 * 60 * 60 * 1000;
      var mistiming = Math.round((Date.now() - timestamp) / 1000);
      var arrr = ['年', '个月', '周', '天', '小时', '分钟', '秒'];
      var arrn = [31536000, 2592000, 604800, 86400, 3600, 60, 1];
      for (var i = 0; i < arrn.length; i++) {
        var inm = Math.floor(mistiming / arrn[i]);
        if (inm !== 0) {
          return inm + arrr[i] + '前';
        }
      }
    }
    return (
      <div>
        {articlelist && articlelist.length ? (
          articlelist.map((item, index) => {
            return (
              <ul key={index} onClick={this.toacticle.bind(this, item)}>
                <li>{item.title}</li>
                <li> · </li>
                <li>{utc2timestamp(item.updateAt)}</li>
              </ul>
            );
          })
        ) : (
          <div>暂无数据</div>
        )}
      </div>
    );
  }
}

export default withRouter(Content);
