import React, { Component } from 'react';
import listData from '../page/File/Fileyh';
import { NavLink } from 'react-router-dom';

class Filearticle extends Component {
  state = {
    list: listData,
  };
  render() {
    const { list } = this.state;
    console.log(list);
    return (
      <div className="buttomrightFile">
        <div className="xbuttom">
          <span>文章分类</span>
        </div>
        <div className="xx">
          <ul>
            {list && list.length
              ? list.map((item, index) => {
                  return (
                    <NavLink to={item.router} key={index}>
                      <li>
                        <span>{item.leading}</span>
                        <span>{item.sumto}</span>
                      </li>
                    </NavLink>
                  );
                })
              : '暂无数据'}
          </ul>
        </div>
      </div>
    );
  }
}
export default Filearticle;
