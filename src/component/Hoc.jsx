import React, { Component } from 'react';
import { message } from 'antd';
export default function Hoc(Com) {
  return class Hoc extends Component {
    componentDidMount() {
      let token = localStorage.getItem('Token');
      if (!token) {
        message.error('重新登录');
      }
    }
    render() {
      return <Com {...this.props}></Com>;
    }
  };
}
