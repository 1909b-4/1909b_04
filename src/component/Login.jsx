import React, { Component } from 'react';
import { Button, Form, Input, Tabs, message } from 'antd';
const { TabPane } = Tabs;
export class Login extends Component {
  state = {
    isModalVisible: false,
  };
  handleOk() {
    this.setState({
      isModalVisible: false,
    });
  }
  handleCancel() {
    this.setState({
      isModalVisible: false,
    });
  }
  onFinish(values) {
    // console.log('Success:', values);
    if (values.username === 'lhz' && values.password === '123') {
      localStorage.setItem('Token', JSON.stringify(values));
      message.success('登录成功');
    } else {
      message.error('登录失败');
    }
  }
  onFinishFailed() {}
  onChange(key) {
    console.log(key);
  }
  render() {
    return (
      <div>
        <Tabs defaultActiveKey="1" onChange={this.onChange.bind(this)}>
          <TabPane tab="登录" key="1">
            <Form
              name="basic"
              labelCol={{
                span: 8,
              }}
              wrapperCol={{
                span: 16,
              }}
              initialValues={{
                remember: true,
              }}
              onFinish={this.onFinish.bind(this)}
              onFinishFailed={this.onFinishFailed.bind(this)}
              autoComplete="off"
            >
              <Form.Item
                label="账户"
                name="username"
                rules={[
                  {
                    required: true,
                    message: 'Please input your username!',
                  },
                ]}
              >
                <Input />
              </Form.Item>

              <Form.Item
                label="密码"
                name="password"
                rules={[
                  {
                    required: true,
                    message: 'Please input your password!',
                  },
                ]}
              >
                <Input.Password />
              </Form.Item>
              <Form.Item
                wrapperCol={{
                  offset: 8,
                  span: 16,
                }}
              >
                <Button type="primary" htmlType="submit">
                  登录
                </Button>
              </Form.Item>
            </Form>
          </TabPane>
          <TabPane tab="注册" key="2">
            <Form
              name="basic"
              labelCol={{
                span: 8,
              }}
              wrapperCol={{
                span: 16,
              }}
              initialValues={{
                remember: true,
              }}
              onFinish={this.onFinish.bind(this)}
              onFinishFailed={this.onFinishFailed.bind(this)}
              autoComplete="off"
            >
              <Form.Item
                label="账户"
                name="username"
                rules={[
                  {
                    required: true,
                    message: 'Please input your username!',
                  },
                ]}
              >
                <Input />
              </Form.Item>
              <Form.Item
                label="密码"
                name="password"
                rules={[
                  {
                    required: true,
                    message: 'Please input your password!',
                  },
                ]}
              >
                <Input.Password />
              </Form.Item>
              <Form.Item
                label="确认密码"
                name="againpassword"
                rules={[
                  {
                    required: true,
                    message: 'Please input your password!',
                  },
                ]}
              >
                <Input.Password />
              </Form.Item>
              <Form.Item
                wrapperCol={{
                  offset: 8,
                  span: 16,
                }}
              >
                <Button>注册</Button>
              </Form.Item>
            </Form>
          </TabPane>
        </Tabs>
      </div>
    );
  }
}

export default Login;
