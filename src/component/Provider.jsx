import React, { Component } from 'react';
import '../styles/provider.less';
import { WifiOutlined, GithubOutlined } from '@ant-design/icons';

class Provider extends Component {
  render() {
    return (
      <div className="provider">
        <p>
          <a href="https://blog.wipi.tech/rss">
            <WifiOutlined />
          </a>
          <a href="https://github.com/fantasticit/wipi">
            <GithubOutlined />
          </a>
        </p>
        <p>Copyright © 2022.All Right Reserved </p>
        <p>后台管理 皖IPC备18005737号</p>
      </div>
    );
  }
}
export default Provider;
