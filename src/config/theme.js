export const dark = {
  '@bg-color': '#000',
  '@link-color': '#fff',
};
export const light = {
  '@bg-color': '#fff',
  '@link-color': '#000',
};
