const data = {
  article: 'article',
  category: 'category',
  knowledge: 'knowledge',
  message: 'message',
  about: 'about',
};

export default data;
