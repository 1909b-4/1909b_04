const data = {
  article: '文章',
  category: '归档',
  knowledge: '知识小册',
  message: '留言板',
  about: '关于',
};

export default data;
