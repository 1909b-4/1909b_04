import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import { Provider } from 'react-redux';
import Redstore from './store/ReduxIndex';
const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <Provider store={Redstore}>
    <App />
  </Provider>
);
document.title = 'FOUR小楼又清风';
