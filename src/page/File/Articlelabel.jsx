const label = [
  {
    router: '/Index/Home',
    leading: '所有',
  },
  {
    router: '/Index/category/fe',
    leading: '前端',
  },
  {
    router: '/Index/category/be',
    leading: '后端',
  },
  {
    router: '/Index/category/reading',
    leading: '阅读',
  },
  {
    router: '/Index/category/leetcode',
    leading: 'LeetCode',
  },
  {
    router: '/Index/category/news',
    leading: '要闻',
  },
  {
    router: '/Index/category/linux',
    leading: '八维',
  },
];
export default label;
