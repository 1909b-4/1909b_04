import Fe from '../home/category/Fe';

const list = [
  {
    router: '/Index/category/fe',
    leading: '前端',
    sumto: `共计 6 篇文章`,
  },
  {
    router: '/Index/category/be',
    leading: '后端',
    sumto: '共计 2 篇文章',
  },
  {
    router: '/Index/category/reading',
    leading: '阅读',
    sumto: '共计 3 篇文章',
  },
  {
    router: '/Index/category/linux',
    leading: 'Linux',
    sumto: '共计 0 篇文章',
  },
  {
    router: '/Index/category/leetcode',
    leading: 'LeetCode',
    sumto: '共计 0 篇文章',
  },
  {
    router: '/Index/category/news',
    leading: '要闻',
    sumto: '共计 0 篇文章',
  },
];
export default list;
