import React, { Component } from 'react';
//引用样式
import '../index.css';
import '../styles/common.less';
//引入 背景色跟字体颜色
import { dark, light } from '../config/theme';
//引用antd组件
import { Modal, Input, Select } from 'antd';
import RouteView from '../router/RouteView';
import { NavLink } from 'react-router-dom';
import Imgs from '../img/icon.png';
import Login from '../../src/component/Login';
// import I18n from "../component/i18n/i18n"
import { inject, observer } from 'mobx-react';
import { FormattedMessage } from 'react-intl';

//引入封装api获取数据组件
import api from '../api/index';
const { Option } = Select;
const { Search } = Input;

class Index extends Component {
  state = {
    flag: false,
    isModalVisible: false,
    searchModal: false,
    nav: [
      {
        to: '/Index/Home',
        name: 'article',
      },
      {
        to: '/Index/File',
        name: 'category',
      },
      {
        to: '/Index/Property',
        name: 'knowledge',
      },
      {
        to: '/Index/Message',
        name: 'message',
      },
      {
        to: '/Index/About',
        name: 'about',
      },
    ],
    list: [],
    arr: [],
  };
  //获取数据
  componentDidMount() {
    api.getArticle().then((res) => {
      this.setState({
        list: res.data[0],
      });
    });
  }
  //点击切换背景颜色
  handClick() {
    let { flag } = this.state;
    this.setState(
      {
        flag: !flag,
      },
      () => {
        window.less.modifyVars(this.state.flag ? dark : light);
      }
    );
  }
  loginbtn() {
    this.setState({
      isModalVisible: true,
    });
  }
  handleOk() {
    this.setState({
      isModalVisible: false,
    });
  }
  handleCancel() {
    this.setState({
      isModalVisible: false,
    });
  }
  searcheOk() {
    this.setState({
      searchModal: false,
    });
  }
  searcheCancel() {
    this.setState({
      searchModal: false,
    });
  }
  searchBtn() {
    this.setState({
      searchModal: true,
    });
  }
  //点击搜索按钮
  onSearch(value) {
    console.log(value);
    this.setState({
      //数据筛选
      arr: this.state.list.filter((item) => {
        return item.title.toString().includes(value);
      }),
    });
  }
  handleClickLange() {
    this.props.titleStore.changeLange();
    console.log(
      this.props.titleStore.message.Target,
      'this.props.titleStore.lange'
    );
  }
  render() {
    // console.log(I18n,'13451');
    console.log(this.props.titleStore.lange, '41');
    let { flag, arr } = this.state;
    return (
      <div style={{ width: '100%', height: '100%' }} className="wrap">
        {/* 登录弹框 */}
        <Modal
          visible={this.state.isModalVisible}
          footer={null}
          onOk={this.handleOk.bind(this)}
          onCancel={this.handleCancel.bind(this)}
        >
          <Login></Login>
        </Modal>
        {/* 搜索弹框 */}
        <Modal
          title="文章搜索"
          visible={this.state.searchModal}
          onOk={this.searcheOk.bind(this)}
          onCancel={this.searcheCancel.bind(this)}
          footer={null}
        >
          <Search
            placeholder="请输入关键字、搜索文章"
            onSearch={this.onSearch.bind(this)}
            enterButton
          />
          {/* 搜索数据渲染 */}
          <div>
            {arr && arr.length ? (
              arr.map((item, index) => {
                return <p key={index}>{item.title}</p>;
              })
            ) : (
              <div>暂无数据</div>
            )}
          </div>
        </Modal>
        {/* 头部布局 */}
        <div className="header-container">
          <div className="logo">
            <NavLink to="/Index/Home">
              <img src={Imgs} alt="" height="60px" />
            </NavLink>
          </div>
          <div className="header-right">
            {/* 渲染导航栏数据 */}
            <div className="nav">
              {this.state.nav.map((item, index) => {
                return (
                  <span key={index}>
                    <NavLink to={item.to}>
                      <FormattedMessage id={item.name} />
                    </NavLink>
                  </span>
                );
              })}
            </div>
            <div className="style">
              <span className="ment">
                <button onClick={this.handleClickLange.bind(this)}>
                  {this.props.titleStore.lange == 'zh-CN' ? '中文' : '英文'}
                </button>
                {/* <Select defaultValue="中文" style={{ width: 75 }}>
                  <Option value="中文">中文</Option>
                  <Option value="英文">英文</Option>
                </Select> */}
              </span>
              <span onClick={this.handClick.bind(this)}>
                {flag ? (
                  <svg
                    className="sun"
                    style={{ width: '24px', height: '24px' }}
                    viewBox="0 0 24 24"
                    title="亮色主题"
                  >
                    <path
                      fill="currentColor"
                      d="M3.55,18.54L4.96,19.95L6.76,18.16L5.34,16.74M11,22.45C11.32,22.45 13,22.45 13,22.45V19.5H11M12,5.5A6,6 0 0,0 6,11.5A6,6 0 0,0 12,17.5A6,6 0 0,0 18,11.5C18,8.18 15.31,5.5 12,5.5M20,12.5H23V10.5H20M17.24,18.16L19.04,19.95L20.45,18.54L18.66,16.74M20.45,4.46L19.04,3.05L17.24,4.84L18.66,6.26M13,0.55H11V3.5H13M4,10.5H1V12.5H4M6.76,4.84L4.96,3.05L3.55,4.46L5.34,6.26L6.76,4.84Z"
                    ></path>
                  </svg>
                ) : (
                  <svg
                    className="moon"
                    style={{ width: '24px', height: '24px' }}
                    viewBox="0 0 24 24"
                    color="deeppink"
                    title="暗色主题"
                  >
                    <path
                      fill="currentColor"
                      d="M17.75,4.09L15.22,6.03L16.13,9.09L13.5,7.28L10.87,9.09L11.78,6.03L9.25,4.09L12.44,4L13.5,1L14.56,4L17.75,4.09M21.25,11L19.61,12.25L20.2,14.23L18.5,13.06L16.8,14.23L17.39,12.25L15.75,11L17.81,10.95L18.5,9L19.19,10.95L21.25,11M18.97,15.95C19.8,15.87 20.69,17.05 20.16,17.8C19.84,18.25 19.5,18.67 19.08,19.07C15.17,23 8.84,23 4.94,19.07C1.03,15.17 1.03,8.83 4.94,4.93C5.34,4.53 5.76,4.17 6.21,3.85C6.96,3.32 8.14,4.21 8.06,5.04C7.79,7.9 8.75,10.87 10.95,13.06C13.14,15.26 16.1,16.22 18.97,15.95M17.33,17.97C14.5,17.81 11.7,16.64 9.53,14.5C7.36,12.31 6.2,9.5 6.04,6.68C3.23,9.82 3.34,14.64 6.35,17.66C9.37,20.67 14.19,20.78 17.33,17.97Z"
                    ></path>
                  </svg>
                )}
              </span>
              <span className="sreach" onClick={this.searchBtn.bind(this)}>
                搜索
              </span>
              <span className="tologin" onClick={this.loginbtn.bind(this)}>
                登录
              </span>
            </div>
          </div>
        </div>
        <div className="container">
          <RouteView router={this.props.router}></RouteView>
        </div>
      </div>
    );
  }
}
// export default Index
export default inject('titleStore')(observer(Index));
