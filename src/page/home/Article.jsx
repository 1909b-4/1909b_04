import React, { Component } from 'react';
import '../../styles/article.less';
import api from '../../api/index';
import {
  HeartTwoTone,
  WechatFilled,
  ShareAltOutlined,
  HeartOutlined,
  EyeOutlined,
  BranchesOutlined,
} from '@ant-design/icons';
import { Badge, Modal } from 'antd';
import { QRCodeSVG } from 'qrcode.react';
import { BackTop } from 'antd';
import markdown from '../../styles/markdown.less';
import classname from 'classnames';

class Article extends Component {
  state = {
    articlelist: [],
    dates: '',
    num: 0,
    isModalVisible: false,
    nums: 0,
  };
  componentDidMount() {
    // api.getRecommend().then((res) => {
    //   this.setState({
    //     articlelist: res.data,
    //   });
    // });
    api.getArticle().then((res) => {
      // console.log(res.data[0], 'data');
      this.setState({
        articlelist: res.data[0],
      });
    });
    // =====================别改=================
    function utc2beijing(date) {
      var T_pos = date.indexOf('T');
      var Z_pos = date.indexOf('Z');
      var year_month_day = date.substr(0, T_pos);
      var hour_minute_second = date.substr(T_pos + 1, Z_pos - T_pos - 1);
      var new_datetime = year_month_day + ' ' + hour_minute_second; // 2017-03-31 08:02:06
      var timestamp;
      timestamp = new Date(Date.parse(new_datetime));
      timestamp = timestamp.getTime();
      timestamp = timestamp / 1000;

      timestamp = timestamp + 8 * 60 * 60;

      var beijing_datetime = new Date(parseInt(timestamp) * 1000)
        .toLocaleString()
        .replace(/年|月/g, '-')
        .replace(/日/g, ' ');
      return beijing_datetime; // 2017-03-31 16:02:06
    }
    this.setState({
      dates: utc2beijing(this.props.location.state.updateAt),
    });
    // =====================别改===========================
  }
  toacticle(item) {
    // console.log(item);.
    this.props.history.push('/Index/article', item);
  }
  NumBtn(num) {
    // console.log(num);
    this.setState({
      num: num + 1,
    });
  }
  showModal() {
    this.setState({
      isModalVisible: true,
    });
  }
  handleOk() {
    this.setState({
      isModalVisible: false,
    });
  }
  handleCancel() {
    this.setState({
      isModalVisible: false,
    });
  }
  toTop() {
    // console.log(111);
    window.scrollTo(0, 0);
  }
  render() {
    let { articlelist, dates } = this.state;
    console.log(articlelist);
    let { state } = this.props.location;
    function utc2timestamp(utc_datetime) {
      var T_pos = utc_datetime.indexOf('T');
      var Z_pos = utc_datetime.indexOf('Z');
      var year_month_day = utc_datetime.substr(0, T_pos);
      var hour_minute_second = utc_datetime.substr(
        T_pos + 1,
        Z_pos - T_pos - 1
      );
      var new_datetime = year_month_day + ' ' + hour_minute_second; // 2017-03-31 08:02:06
      var timestamp;
      timestamp = new Date(Date.parse(new_datetime));
      timestamp = timestamp.getTime();
      timestamp = timestamp + 8 * 60 * 60 * 1000;
      var mistiming = Math.round((Date.now() - timestamp) / 1000);
      var arrr = ['年', '个月', '周', '天', '小时', '分钟', '秒'];
      var arrn = [31536000, 2592000, 604800, 86400, 3600, 60, 1];
      for (var i = 0; i < arrn.length; i++) {
        var inm = Math.floor(mistiming / arrn[i]);
        if (inm !== 0) {
          return inm + arrr[i] + '前';
        }
      }
    }
    return (
      <div className="article">
        <Modal
          title="分享海报"
          visible={this.state.isModalVisible}
          onOk={this.handleOk.bind(this)}
          onCancel={this.handleCancel.bind(this)}
        >
          {state.cover ? (
            <img src={state.cover} alt="" width="95%" height="200px" />
          ) : (
            ''
          )}
          <div>
            <span style={{ fontWeight: 'bold' }}>{state.title}</span>
          </div>
          <div>
            <span>{state.summary}</span>
          </div>
          <div>
            <QRCodeSVG value="https://reactjs.org/" />
            <span>识别二维码查看文章</span>
          </div>
        </Modal>
        <BackTop />
        <div className="homemain" style={{ display: 'flex' }}>
          <div className="icon">
            <Badge count={this.state.num}>
              <HeartTwoTone
                twoToneColor="#eb2f96"
                onClick={this.NumBtn.bind(this, this.state.num)}
                style={{ fontSize: '18px' }}
              />
            </Badge>
            <WechatFilled
              style={{ fontSize: '18px' }}
              className="WechatFilled"
            />
            <ShareAltOutlined
              style={{ fontSize: '18px' }}
              onClick={this.showModal.bind(this)}
            />
          </div>
          <div className="left">
            <div className="articleimg">
              {state.cover ? <img src={state.cover} alt="" /> : ''}
            </div>
            <div className="articletit">
              <h3>{state.title}</h3>
              <i>
                发布于 {dates} · 阅读量 {state.views}
              </i>
            </div>
            <div
              className={classname('markdown')}
              dangerouslySetInnerHTML={{ __html: state.html }}
            ></div>
            <div className="homefooter">
              <p>推荐阅读</p>
              <div className="homefootermain">
                {articlelist && articlelist.length ? (
                  articlelist.map((item, index) => {
                    return (
                      <dl key={index} onClick={this.toacticle.bind(this, item)}>
                        <dd>
                          <strong
                            className="pone"
                            onClick={this.toTop.bind(this)}
                          >
                            <b>{item.title}</b>
                            <b>{utc2timestamp(item.updateAt)}</b>
                            <b>{item.category ? item.category.label : ''}</b>
                          </strong>
                          <p className="ptwo" onClick={this.toTop.bind(this)}>
                            {item.summary}
                          </p>
                          <p className="pthree">
                            <b>
                              <HeartOutlined />
                              {item.likes}
                            </b>
                            <b>·</b>
                            <b>
                              <EyeOutlined />
                              {item.views}
                            </b>
                            <b>·</b>
                            <b>
                              <BranchesOutlined />
                              分享
                            </b>
                          </p>
                        </dd>
                        <dt>
                          {item.cover ? <img src={item.cover} alt="" /> : ''}
                        </dt>
                      </dl>
                    );
                  })
                ) : (
                  <div>暂无数据</div>
                )}
              </div>
            </div>
          </div>
          <div className="right">
            {/* <div className="rightdiv"> */}
              {/* @rts-ignore */}
              <div className="recommend">
                <div className="recommendnav">
                  <h5>推荐阅读</h5>
                </div>
                <div className="recommendmain">
                  {articlelist && articlelist.length ? (
                    articlelist.map((item, index) => {
                      return (
                        <ul
                          key={index}
                          onClick={this.toacticle.bind(this, item)}
                        >
                          <li>{item.title}</li>
                          <li> · </li>
                          <li>{utc2timestamp(item.updateAt)}</li>
                        </ul>
                      );
                    })
                  ) : (
                    <div>暂无数据</div>
                  )}
                </div>
              </div>
              <div className="stair">楼层</div>
            </div>
          {/* </div> */}
        </div>
        {/* <div className="providerFooter">
          <div className="provider">
            <p>
              <a href="https://blog.wipi.tech/rss">
                <WifiOutlined />
              </a>
              <a href="https://github.com/fantasticit/wipi">
                <GithubOutlined />
              </a>
            </p>
            <p>Copyright © 2022.All Right Reserved </p>
            <p>后台管理 皖IPC备18005737号</p>
          </div>
        </div> */}
        <strong className="site-back-top-basic"></strong>
      </div>
    );
  }
}
export default Article;
