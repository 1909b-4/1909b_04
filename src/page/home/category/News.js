import React, { Component } from 'react';
import '../../../styles/category.less';
import '../../../styles/home.less';
import api from '../../../api/index';
import {
  HeartOutlined,
  EyeOutlined,
  BranchesOutlined,
} from '@ant-design/icons';

class News extends Component {
  state = {
    articlelist: [],
    fsList: [],
  };
  componentDidMount() {
    api.getArticle().then((res) => {
      this.setState({
        articlelist: res.data[0],
        fsList: res.data[0].filter((item) => {
          if (item.category && item.category.label === '要闻') {
            return item;
          }
        }),
      });
    });
  }
  toacticle(item) {
    // console.log(item);.
    this.props.history.push('/Index/article', item);
  }
  render() {
    let { articlelist, fsList } = this.state;
    console.log(fsList);
    return (
      <div className="category">
        <div className="homemain">
          <div className="left">
            <div className="categoryclass">
              <p>
                <span>要闻</span>
                <span>分享文章</span>
              </p>
              <p>
                共搜索到 <span>{fsList.length}</span> 篇
              </p>
            </div>
            <div className="lefttxt">
              <div className="leftdown">
                <header>
                  <a href="/Index/Home">所有</a>
                  <a href="/Index/category/fe">前端</a>
                  <a href="/Index/category/be">后端</a>
                  <a href="/Index/category/reading">阅读</a>
                  <a href="/Index/category/leetcode">LeetCode</a>
                  <a href="/Index/category/news">要闻</a>
                  <a href="/Index/category/linux">八维</a>
                </header>
                <main>
                  {fsList && fsList.length ? (
                    fsList.map((item, index) => {
                      return (
                        <dl
                          key={index}
                          onClick={this.toacticle.bind(this, item)}
                        >
                          <dd>
                            <p className="pone">
                              <span>{item.title}</span>
                              <span>发布时间</span>
                              <span>
                                {item.category ? item.category.label : ''}
                              </span>
                            </p>
                            <p className="ptwo">{item.summary}</p>
                            <p className="pthree">
                              <span>
                                <HeartOutlined />
                                {item.likes}
                              </span>
                              <b>·</b>
                              <span>
                                <EyeOutlined />
                                {item.views}
                              </span>
                              <b>·</b>
                              <span>
                                <BranchesOutlined />
                                分享
                              </span>
                            </p>
                          </dd>
                          <dt>
                            {item.cover ? <img src={item.cover} alt="" /> : ''}
                          </dt>
                        </dl>
                      );
                    })
                  ) : (
                    <div>loadding....</div>
                  )}
                </main>
              </div>
            </div>
          </div>
          <div className="right">
            <div className="recommend">
              <div className="recommendnav">
                <h5>推荐阅读</h5>
              </div>
              <div className="recommendmain">
                {articlelist && articlelist.length ? (
                  articlelist.map((item, index) => {
                    return (
                      <ul key={index} onClick={this.toacticle.bind(this, item)}>
                        <li>{item.title}</li>
                        <li> · </li>
                        <li>大约几月前</li>
                      </ul>
                    );
                  })
                ) : (
                  <div>暂无数据</div>
                )}
              </div>
            </div>
            {/* <div className='label'>

                        </div>
                        <div className='provider'></div> */}
          </div>
        </div>
      </div>
    );
  }
}

export default News;
