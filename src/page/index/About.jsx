import React, { Component } from 'react';
import axios from 'axios';
import { Input, Checkbox, Image, Modal, BackTop, Pagination } from 'antd';
import {
  MessageOutlined,
  SmileOutlined,
  UserOutlined,
  HeartOutlined,
  EyeOutlined,
  ShareAltOutlined,
} from '@ant-design/icons';

import Login from '../../component/Login';
import '../../styles/about.less';
import title from '../../utils/time';
import { QRCodeCanvas } from 'qrcode.react';
const { TextArea } = Input;
export class About extends Component {
  state = {
    list: [],
    arr: [],
    values: '',
    isModalVisible: 0,
    object: {},
    minValue: 0,
    maxValue: 6,
    defaultPageSize: 6,
  };
  componentDidMount() {
    axios.get('/api/article').then((res) => {
      this.setState({
        list: res.data.data,
      });
    });
    axios.get('/api/comment').then((res) => {
      // console.log(res.data.data)
      this.setState({
        arr: res.data.data,
      });
    });
  }
  detalis(item) {
    this.props.history.push('/Index/article', item);
  }
  Fa() {
    console.log(123);
    this.setState({
      isModalVisible: true,
    });
  }
  handleOk() {
    this.setState({
      isModalVisible: 0,
    });
  }
  handleCancel() {
    this.setState({
      isModalVisible: 0,
    });
  }
  add() {
    this.setState({
      isModalVisible: 2,
    });
  }
  search() {
    this.setState({
      isModalVisible: 1,
    });
  }
  share(e, item) {
    this.setState({
      isModalVisible: 3,
      object: item,
    });
    e.stopPropagation();
  }
  handleValue = (value) => {
    // console.log(value)
    if (value <= 1) {
      this.setState({
        minValue: 0,
        maxValue: 6,
      });
    } else {
      this.setState({
        minValue: (value - 1) * 6,
        maxValue: (value - 1) * 6 + 6,
      });
    }
  };
  render() {
    let { list, arr, isModalVisible, object, num, defaultPageSize } =
      this.state;
    return (
      <div className="about">
        <BackTop />
        <div className="about_head">
          <div>
            <Image
              width={200}
              src="https://wipi.oss-cn-shanghai.aliyuncs.com/2020-04-04/despaired-2261021_1280.jpg"
            />
            {/* <img src="https://wipi.oss-cn-shanghai.aliyuncs.com/2020-04-04/despaired-2261021_1280.jpg" /> */}
          </div>

          <div className="about_markdown">
            <h1>
              这世界只有一种英雄主义,就是在看清了生活的真相之后,依然热爱生活
            </h1>
            <h2>更新</h2>
            <hr />
            <ul>
              <li>
                <p>
                  新增Githu OAuth 登录，由于网路原因,容易失败,可以多尝试几下
                </p>
              </li>
              <li>
                <Checkbox defaultChecked disabled />1
              </li>
              <li>
                <Checkbox defaultChecked={false} disabled />2
              </li>
            </ul>
          </div>
        </div>
        <div className="about_bottom">
          <div className="about_review">
            <p className="comment">评论</p>
            <div className="comment_button">
              <div className="comment_textbox">
                <div>
                  <UserOutlined className="info"></UserOutlined>
                </div>
                <p>
                  <TextArea
                    onClick={this.search.bind(this)}
                    rows={4}
                    placeholder="请输入评论内容 (支持 Markdown)"
                  />
                </p>
              </div>
              <div className="comment_issue">
                <div>
                  <p className="expression" onClick={this.add.bind(this)}>
                    <SmileOutlined />
                    表情
                  </p>
                  <button>发 布</button>
                </div>
              </div>
            </div>
            {/* 登录对话框 */}
            <Modal
              visible={isModalVisible === 1}
              footer={null}
              onOk={this.handleOk.bind(this)}
              onCancel={this.handleCancel.bind(this)}
            >
              <Login></Login>
            </Modal>
            {/* 表情对话框 */}
            <Modal
              visible={isModalVisible === 2}
              onOk={this.handleOk.bind(this)}
              onCancel={this.handleCancel.bind(this)}
            >
              <span>
                <SmileOutlined />
              </span>
              <span>
                <HeartOutlined />
              </span>
              <span>
                <ShareAltOutlined />
              </span>
            </Modal>
            {/* 分享对话框 */}
            <Modal
              visible={isModalVisible === 3}
              width={400}
              title="分享海报"
              onOk={this.handleOk.bind(this)}
              onCancel={this.handleCancel.bind(this)}
            >
              <p className="modal">
                <img src={object.cover} alt="" />
              </p>
              <div>
                <b>{object.title}</b>
              </div>
              <div>{object.summary}</div>
              <div className="mode_code">
                <div>
                  <QRCodeCanvas value="https://reactjs.org/" />
                </div>
                <div className="qrcode">
                  <div>识别二维码查看文章</div>
                  <div>
                    原文分享自
                    <span onClick={this.detalis.bind(this, object)}>
                      小楼又清风
                    </span>
                  </div>
                </div>
              </div>
            </Modal>
            <div className="comment_portrait">
              {arr && arr.length
                ? arr[0]
                    .slice(this.state.minValue, this.state.maxValue)
                    .map((item, index) => {
                      return (
                        <div key={index} className="comment_tou">
                          <dl>
                            <dt>
                              <p>头</p>
                              <div>{item.name}</div>
                            </dt>
                            <dd>
                              <p>{item.content}</p>
                              <p>
                                <span>{item.userAgent}</span>
                                <span>大约{title(item.updateAt)}</span>
                                <span>
                                  <MessageOutlined />
                                  回复
                                </span>
                              </p>
                            </dd>
                          </dl>
                        </div>
                      );
                    })
                : ''}
              {/* 分页 */}
              <div className="paging">
                <Pagination
                  defaultCurrent={num}
                  defaultPageSize={defaultPageSize}
                  total={arr && arr.length ? arr[0].length : ''}
                  onChange={this.handleValue.bind(this)}
                />
              </div>
            </div>
          </div>
        </div>
        <div className="about_read">
          <div className="about_inspect">
            <div className="suggested">推荐阅读</div>
            {list && list.length
              ? list[0].map((item, index) => {
                  return (
                    <div
                      className="recommend"
                      key={index}
                      onClick={this.detalis.bind(this, item)}
                    >
                      <header>
                        <span>
                          <b>{item.title}</b>
                        </span>
                        <span>
                          {item.category ? item.category.createAt : ''}
                        </span>
                        <span>{item.category ? item.category.label : ''}</span>
                      </header>
                      <main>
                        <div className="about_left">
                          <div>{item.summary}</div>
                          <div>
                            <span>
                              <HeartOutlined />
                              {item.likes}
                            </span>
                            <span>
                              <EyeOutlined />
                              {item.views}
                            </span>
                            <span onClick={(e) => this.share(e, item)}>
                              <ShareAltOutlined />
                              分享
                            </span>
                          </div>
                        </div>
                        <div className="about_right">
                          <img src={item.cover} alt="" />
                        </div>
                      </main>
                    </div>
                  );
                })
              : ''}
          </div>
        </div>
        <strong className="site-back-top-basic"></strong>
      </div>
    );
  }
}

export default About;
