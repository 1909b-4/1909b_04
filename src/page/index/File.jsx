import React, { Component } from 'react';
import '../../styles/file.less';
import BS from 'better-scroll';
import { Sticky } from 'react-vant';
import { BackTop } from 'antd';
import { WifiOutlined, GithubOutlined } from '@ant-design/icons';
import Filearticle from '../../component/Filearticle';
import api from '../../api/index';
import Content from '../../component/Content';

const style = {
  width: '100%',
  height: '100%',
  position: 'relative',
};
export class File extends Component {
  state = {
    arr: [],
    changeIndex: 0,
    isloading: false,
    num: 0,
  };
  componentDidMount() {
    api.getArticle().then((res) => {
      this.setState({
        arr: res.data[0],
      });
    });
    this.BS = new BS('.buttonleftFile', {
      pullDownRefresh: true,
      click: false,
    });
    this.BS.on('pullingDown', () => {
      this.setState({
        isloading: true,
      });
      this.state.arr('down');
      setTimeout(() => {
        this.BS.refresh();
        this.BS.finishPullDown();
        this.setState({
          isloading: false,
        });
      }, 2000);
    });
  }
  componentWillUnmount() {
    this.BS.destroy();
  }
  check(index) {
    this.setState({
      changeIndex: index,
    });
  }
  routerTo(item) {
    this.props.history.push('/Index/article', item);
  }
  render() {
    const { arr } = this.state;
    return (
      <div style={style}>
        <div className="containerFile">
          <BackTop />
          <div className="leftFile">
            <div className="topleftFile">
              <span>归档</span>
              <p>
                共计<b style={{ color: 'red' }}>{arr.length}</b>篇
              </p>
            </div>
            <div className="buttonleftFile">
              <div className="refresh">
                <span>2022</span>
                <p>May</p>
                <ul>
                  {arr && arr.length
                    ? arr.map((item, index) => {
                        return (
                          <li
                            key={item.id}
                            onClick={this.check.bind(this, index)}
                            className={
                              index === this.state.changeIndex
                                ? 'activemain'
                                : ''
                            }
                          >
                            <div onClick={this.routerTo.bind(this, item)}>
                              <span>
                                <time dateTime="04-12">04-12</time>
                              </span>
                              <span>{item.title}</span>
                            </div>
                          </li>
                        );
                      })
                    : '暂无数据'}
                  {this.state.isloading ? <div>文章页面加载中...</div> : ''}
                </ul>
              </div>
            </div>
          </div>

          <div className="rightFile">
            <Sticky offsetTop={5}>
              <div className="toprightFile">
                <div className="stop">
                  <span>推荐阅读</span>
                </div>
                <Content></Content>
              </div>

              <Filearticle></Filearticle>
              <div className="provider">
                <p>
                  <a href="https://blog.wipi.tech/rss">
                    <WifiOutlined />
                  </a>
                  <a href="https://github.com/fantasticit/wipi">
                    <GithubOutlined />
                  </a>
                </p>
                <p>Copyright © 2022.All Right Reserved </p>
                <p>后台管理 皖IPC备18005737号</p>
              </div>
            </Sticky>
          </div>
        </div>
        <strong className="site-back-top-basic"> </strong>
      </div>
    );
  }
}

export default File;
