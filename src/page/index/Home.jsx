import React, { Component } from 'react';
import '../../styles/home.less';
import ArticleLabel from '../../component/ArticleLabel';
import Content from '../../component/Content';
import api from '../../api/index';
import Provider from '../../component/Provider';
import { Modal } from 'antd';
import { QRCodeSVG } from 'qrcode.react';
import {
  HeartOutlined,
  EyeOutlined,
  BranchesOutlined,
} from '@ant-design/icons';
import { Carousel, BackTop } from 'antd';

export class Home extends Component {
  state = {
    articlelist: [],
    isModalVisible: false,
    cover: '',
    summary: '',
    title: '',
  };
  componentDidMount() {
    api.getArticle().then((res) => {
      this.setState({
        articlelist: res.data[0],
      });
    });
  }
  onChange(currentSlide) {
    // console.log(currentSlide);
  }
  toacticle(item) {
    this.props.history.push('/Index/article', item);
  }
  totop() {
    document.body.scrollTop = 0;
  }
  shere(item, e) {
    e.stopPropagation();
    // console.log(e)
    this.setState({
      isModalVisible: true,
      cover: item.cover,
      summary: item.summary,
      title: item.title,
    });
  }
  handleOk() {
    this.setState({
      isModalVisible: false,
    });
  }
  handleCancel() {
    this.setState({
      isModalVisible: false,
    });
  }
  render() {
    let { articlelist, cover, summary, title } = this.state;
    console.log(articlelist);
    function utc2timestamp(utc_datetime) {
      var T_pos = utc_datetime.indexOf('T');
      var Z_pos = utc_datetime.indexOf('Z');
      var year_month_day = utc_datetime.substr(0, T_pos);
      var hour_minute_second = utc_datetime.substr(
        T_pos + 1,
        Z_pos - T_pos - 1
      );
      var new_datetime = year_month_day + ' ' + hour_minute_second; // 2017-03-31 08:02:06
      var timestamp;
      timestamp = new Date(Date.parse(new_datetime));
      timestamp = timestamp.getTime();
      timestamp = timestamp + 8 * 60 * 60 * 1000;
      var mistiming = Math.round((Date.now() - timestamp) / 1000);
      var arrr = ['年', '个月', '周', '天', '小时', '分钟', '秒'];
      var arrn = [31536000, 2592000, 604800, 86400, 3600, 60, 1];
      for (var i = 0; i < arrn.length; i++) {
        var inm = Math.floor(mistiming / arrn[i]);
        if (inm !== 0) {
          return inm + arrr[i] + '前';
        }
      }
    }
    return (
      <div className="home">
        <Modal
          title="分享海报"
          visible={this.state.isModalVisible}
          onOk={this.handleOk.bind(this)}
          onCancel={this.handleCancel.bind(this)}
        >
          <img src={cover} alt="" />
          <div>
            <p style={{ fontWeight: 'bold' }}>{title}</p>
            <p>{summary}</p>
          </div>
          <div>
            <QRCodeSVG value="https://reactjs.org/" />
            <span>识别二维码查看文章</span>
          </div>
        </Modal>
        <BackTop />
        <div className="homemain">
          <div className="left">
            <div className="topimg">
              <Carousel afterChange={this.onChange}>
                {articlelist && articlelist.length ? (
                  articlelist.map((item, index) => {
                    return (
                      <div className="banner" key={index}>
                        <img src={item.cover} alt="" />
                        <div className="topzzc">
                          <a onClick={this.toacticle.bind(this, item)}>
                            <h2>{item.title}</h2>
                            <p>
                              <span>
                                <time>大约{utc2timestamp(item.updateAt)}</time>
                              </span>
                              <span> · </span>
                              <span>{item.views} 次 阅 读</span>
                            </p>
                          </a>
                        </div>
                      </div>
                    );
                  })
                ) : (
                  <div>暂无数据</div>
                )}
              </Carousel>
            </div>
            <div className="lefttxt">
              <div className="leftdown">
                <header>
                  <a href="/Index/Home">所有</a>
                  <a href="/Index/category/fe">前端</a>
                  <a href="/Index/category/be">后端</a>
                  <a href="/Index/category/reading">阅读</a>
                  <a href="/Index/category/leetcode">LeetCode</a>
                  <a href="/Index/category/news">要闻</a>
                  <a href="/Index/category/linux">八维</a>
                </header>
                <main>
                  {articlelist && articlelist.length ? (
                    articlelist.map((item, index) => {
                      return (
                        <dl
                          key={index}
                          onClick={this.toacticle.bind(this, item)}
                        >
                          <dd>
                            <p className="pone">
                              <span>{item.title}</span>
                              <span>{utc2timestamp(item.updateAt)}</span>
                              <span>
                                {item.category ? item.category.label : ''}
                              </span>
                            </p>
                            <p className="ptwo">{item.summary}</p>
                            <p className="pthree">
                              <span>
                                <HeartOutlined />
                                {item.likes}
                              </span>
                              <b>·</b>
                              <span>
                                <EyeOutlined />
                                {item.views}
                              </span>
                              <b>·</b>
                              <span onClick={this.shere.bind(this, item)}>
                                <BranchesOutlined />
                                分享
                              </span>
                            </p>
                          </dd>
                          <dt>
                            {item.cover ? <img src={item.cover} alt="" /> : ''}
                          </dt>
                        </dl>
                      );
                    })
                  ) : (
                    <div>暂无数据</div>
                  )}
                </main>
              </div>
            </div>
          </div>
          <div className="right">
            <div className="recommend">
              <div className="recommendnav">
                <h5>推荐阅读</h5>
              </div>
              <div className="recommendmain">
                <Content></Content>
              </div>
            </div>
            <div className="label">
              <div className="labelnav">
                <h5>文章标签</h5>
              </div>
              <div className="labelmain">
                <ArticleLabel></ArticleLabel>
              </div>
            </div>
            {/* 备案号组件 */}
            <Provider />
            {/*  备案号组件*/}
          </div>
        </div>
        <strong className="site-back-top-basic"></strong>
      </div>
    );
  }
}

export default Home;
