import React, { Component } from 'react';
import Prompt from './message/Prompt';
import Board from './message/Board';
import Reading from './message/Reading';
import message from '../../styles/message.module.less';
import api from '../../api/index';
import { BackTop } from 'antd';
export class Message extends Component {
  state = {
    data: [],
  };
  componentDidMount() {
    api.MessList().then((res) => {
      this.setState({
        data: res.data,
      });
    });
  }
  render() {
    let { data } = this.state;
    return (
      <div>
        <BackTop />
        <header>
          <Prompt></Prompt>
        </header>
        <div className={message.MessagePrincipal}>
          <main>
            <Board></Board>
          </main>
        </div>
        <div className={message.MessagePrincipal}>
          <footer>
            <Reading readingdata={data[0]}></Reading>
          </footer>
        </div>
        <strong className="site-back-top-basic"></strong>
      </div>
    );
  }
}

export default Message;
