import React, { Component } from 'react';
import message from '../../../styles/message.module.less';
import { Avatar } from 'antd';
import { SmileOutlined, MessageOutlined } from '@ant-design/icons';
import Expression from './Expression';
import time from '../../../utils/time';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from '../../../action/message';
import { Pagination } from 'antd';

let Mock = require('mockjs');

class Board extends Component {
  state = {
    emojilist: [
      { emoji: '😀' },
      { emoji: '😄' },
      { emoji: '😁' },
      { emoji: '😅' },
      { emoji: '🤣' },
      { emoji: '😂' },
      { emoji: '🙂' },
      { emoji: '😇' },
      { emoji: '😉' },
    ],
    emojidata: [],
    shuru: '',
    flag: false,
    huifu: '',
    huifukuang: 0,
    flag1: false,
  };
  async componentDidMount() {
    this.props.mess_list();
    this.state.emojilist.forEach((item, value) => {
      let emojiid = Mock.mock({
        id: '@id',
      });
      let emojiobj = {
        ...item,
        ...emojiid,
      };
      this.state.emojidata.push(emojiobj);
    });
  }
  shurua(e) {
    this.setState({
      shuru: e.target.value,
    });
  }
  huifua(e) {
    this.setState({
      huifu: e.target.value,
    });
  }
  shangchuan() {
    this.props.liuyan(this.state.shuru);
    this.setState({
      shuru: '',
    });
  }
  yihuifu(value) {
    this.setState({
      flag: !this.state.flag,
      huifukuang: value,
    });
  }
  yijihuifu(value) {
    this.setState({
      huifu: '',
    });
    this.props.yi_huifu({ inde: value, item: this.state.huifu });
  }
  erhuifu(k) {
    this.setState({
      flag1: !this.state.flag1,
      huifukuang: k,
    });
  }
  erjihuifu(item) {
    this.setState({
      huifu: '',
    });
    this.props.er_huifu({ inde: item, item: this.state.huifu });
  }
  dangqian = (page) => {
    this.props.tiao_shu(page);
  };
  emojiqing = (index) => {
    this.setState({
      shuru: this.state.shuru + this.state.emojidata[index].emoji,
    });
  };
  render() {
    let { emojidata, shuru, flag, huifu, huifukuang, flag1 } = this.state;
    let { boarddata, fenye, zongshu, tiaoshu } = this.props;
    return (
      <div>
        <p>评论</p>
        <div className={message.messagejuzhong}>
          <div>
            <div className={message.messageinput}>
              <div className={message.divtou}>
                <Avatar src="https://joeschmoe.io/api/v1/random" />
              </div>
              <div className={message.divshu}>
                <div>
                  <input
                    type="text"
                    value={shuru}
                    onChange={(e) => this.shurua(e)}
                    placeholder="请输入评论内容（支持 Markdown）"
                  />
                </div>
                <div>
                  <div className={message.divfabu}>
                    <p>
                      <SmileOutlined />
                      <Expression
                        emojidata={emojidata}
                        emojibiao={this.emojiqing}
                      />
                    </p>
                    <p>
                      <button
                        className={message.pfabu}
                        onClick={this.shangchuan.bind(this)}
                      >
                        发布
                      </button>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className={message.messagejuzhong}>
          <div>
            {fenye.length && fenye ? (
              fenye.map((item, value) => {
                return (
                  <div key={value} className={message.headermainfooter}>
                    <header className={message.headers}>
                      <span className={message.touxiang1}>
                        <span className={message.touxiang2}>
                          {item.name.substr(0, 1)}
                        </span>
                      </span>
                      <span className={message.plname}>
                        <strong>{item.name}</strong>
                      </span>
                    </header>
                    <main className={message.plmain}>
                      <div
                        dangerouslySetInnerHTML={{ __html: item.html }}
                      ></div>
                    </main>
                    <footer className={message.plfoot}>
                      <div>
                        <span>{item.userAgent}</span>
                        <span>大约{time(item.updateAt)}</span>
                        <span className={message.huifuspan}>
                          <MessageOutlined />
                          <span onClick={this.yihuifu.bind(this, value)}>
                            回复
                          </span>
                        </span>
                      </div>
                      <div
                        style={
                          flag && huifukuang === value
                            ? { display: 'block' }
                            : { display: 'none' }
                        }
                      >
                        <div>
                          <div className={message.messageinput}>
                            <div className={message.divtou}>
                              <Avatar src="https://joeschmoe.io/api/v1/random" />
                            </div>
                            <div className={message.divshu}>
                              <div>
                                <input
                                  type="text"
                                  value={huifu}
                                  onChange={(e) => this.huifua(e)}
                                  placeholder="请输入评论内容（支持 Markdown）"
                                />
                              </div>
                              <div>
                                <div className={message.divfabu}>
                                  <p>
                                    <SmileOutlined />
                                    <Expression emojidata={emojidata} />
                                  </p>
                                  <p>
                                    <button
                                      className={message.pfabu}
                                      onClick={this.yijihuifu.bind(this, value)}
                                    >
                                      回复
                                    </button>
                                  </p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div>
                        {item.children.length && item.children ? (
                          item.children.map((val, k) => {
                            return (
                              <div
                                key={k}
                                className={message.headermainfooter1}
                              >
                                <header className={message.headers1}>
                                  <span className={message.touxiang11}>
                                    <span className={message.touxiang21}>
                                      {val.name.substr(0, 1)}
                                    </span>
                                  </span>
                                  <span className={message.plname1}>
                                    <strong>{val.name}</strong>
                                    <span className={message.huifuhuifu}>
                                      回复
                                    </span>
                                    <strong>{val.replyUserName}</strong>
                                  </span>
                                </header>
                                <main className={message.plmain1}>
                                  <div
                                    dangerouslySetInnerHTML={{
                                      __html: val.html,
                                    }}
                                  ></div>
                                </main>
                                <footer className={message.plfoot1}>
                                  <div>
                                    <span>{val.userAgent}</span>
                                    <span>大约{time(val.updateAt)}</span>
                                    <span className={message.huifuspan1}>
                                      <MessageOutlined />
                                      <span
                                        onClick={this.erhuifu.bind(this, k)}
                                      >
                                        回复
                                      </span>
                                    </span>
                                  </div>
                                  <div
                                    style={
                                      flag1 && huifukuang === k
                                        ? { display: 'block' }
                                        : { display: 'none' }
                                    }
                                  >
                                    <div>
                                      <div className={message.messageinput}>
                                        <div className={message.divtou}>
                                          <Avatar src="https://joeschmoe.io/api/v1/random" />
                                        </div>
                                        <div className={message.divshu}>
                                          <div>
                                            <input
                                              type="text"
                                              value={huifu}
                                              onChange={(e) => this.huifua(e)}
                                              placeholder="请输入评论内容（支持 Markdown）"
                                            />
                                          </div>
                                          <div>
                                            <div className={message.divfabu}>
                                              <p>
                                                <SmileOutlined />
                                                <Expression
                                                  emojidata={emojidata}
                                                />
                                              </p>
                                              <p>
                                                <button
                                                  className={message.pfabu}
                                                  onClick={this.erjihuifu.bind(
                                                    this,
                                                    { value: value, k: k }
                                                  )}
                                                >
                                                  回复
                                                </button>
                                              </p>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </footer>
                              </div>
                            );
                          })
                        ) : (
                          <div></div>
                        )}
                      </div>
                    </footer>
                  </div>
                );
              })
            ) : (
              <div>暂无评论</div>
            )}
          </div>
        </div>
        <Pagination
          size="small"
          pageSize={zongshu}
          current={tiaoshu}
          total={boarddata.length}
          defaultPageSize={3}
          onChange={this.dangqian}
        />
      </div>
    );
  }
}

let mapStateToProps = ({ messageredux }) => {
  return { ...messageredux };
};
let mapDispatchToProps = (dispatch) => {
  return bindActionCreators(actions, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(Board);
