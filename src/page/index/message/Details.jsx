import React, { Component } from 'react';
import message from '../../../styles/message.module.less'
import Likes from './Likes'

class Details extends Component {
    state={
        likeflag:false
    }
    plikes=(item)=>{
        this.setState({
            likeflag:item
        })
    }
    render() {
        let {likeflag}=this.state
        let { detail } = this.props.location.state
        return (
            <div>
                <div className={message.kuaijuzhong}>
                    <div className={message.kuaijuzi}>
                        <div className={message.kuaijuzitu}>
                            <div className={message.kuaijuzitutu}>
                                <img src={detail.cover} alt="" />
                            </div>
                        </div>
                        <div className={message.xuanrank}>
                            <div className={message.xuanrankuai}>
                                <div className={message.detail} dangerouslySetInnerHTML={{ __html: detail.html }}></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={message.detaillike}>
                    <Likes detail={detail} likse={this.plikes}></Likes>
                </div>
            </div>
        );
    }
}

export default Details;
