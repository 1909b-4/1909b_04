import React, { Component } from 'react';
import message from '../../../styles/message.module.less';
import { Modal, Button } from 'antd';

class Expression extends Component {
  state = { visible: false };

  showModal = () => {
    this.setState({
      visible: true,
    });
  };

  handleOk = (e) => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };

  handleCancel = (e) => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };
  xoaop = (value) => {
    this.props.emojibiao(value);
    this.setState({
      visible: false,
    });
  };
  render() {
    let { emojidata } = this.props;
    return (
      <>
        <Button
          type="primary"
          onClick={this.showModal}
          className={message.biaoqing}
        >
          表情
        </Button>
        <Modal
          title="表情"
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
          <div>
            {emojidata.length && emojidata ? (
              emojidata.map((item, value) => {
                return (
                  <span
                    style={{ width: '20px' }}
                    key={value}
                    onClick={() => {
                      this.xoaop(value);
                    }}
                  >
                    {item.emoji}
                  </span>
                );
              })
            ) : (
              <div>暂无表情</div>
            )}
          </div>
        </Modal>
      </>
    );
  }
}

export default Expression;
