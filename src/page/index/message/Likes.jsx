import React, { Component } from 'react';
import {
    HeartOutlined,
    CommentOutlined,
    ShareAltOutlined
} from '@ant-design/icons';
import messagelike from '../../../styles/messagelike.module.less';

class Likes extends Component {
    state={
        detail:this.props.detail,
        flag:false
    }
    likess=(like)=>{
        this.state.flag=!this.state.flag
        if(this.state.flag===true){
            like.likes++
        }else{
            like.likes--
        }
        this.props.likse(this.state.flag)
        this.setState({
            detail:like,
            flag:this.state.flag
        })
    }

    render() {
        let { detail ,flag } = this.state
        return (
            <div className={messagelike.likesbody}>
                <p className={messagelike.likescunt}>{detail.likes}</p>
                <div className={messagelike.likesdiv}>
                    <HeartOutlined onClick={()=>{this.likess(detail)}} style={flag?{color:'red'}:{color:''}}  />
                </div>
                <div className={messagelike.likesdiv}>
                    <CommentOutlined />
                </div>
                <div className={messagelike.likesdiv}>
                    <ShareAltOutlined />
                </div>
            </div>
        );
    }
}

export default Likes;