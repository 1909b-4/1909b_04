import React, { Component } from 'react';
import message from '../../../styles/message.module.less';

class Prompt extends Component {
  render() {
    return (
      <div className={message.header}>
        <p>留言板</p>
        <h4>『请勿灌水』🤖</h4>
        <h4>『垃圾评论过多，欢迎提供好的过滤（标记）算法』</h4>
      </div>
    );
  }
}

export default Prompt;
