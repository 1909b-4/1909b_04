import React, { Component } from 'react';
import message from '../../../styles/message.module.less';
import {
  HeartOutlined,
  EyeOutlined,
  ShareAltOutlined,
} from '@ant-design/icons';
import time from '../../../utils/time';
import { withRouter } from 'react-router-dom';
import { Modal, Button } from 'antd';
import { QRCodeSVG } from 'qrcode.react';

class Reading extends Component {
  state = { visible: false };
  details(item) {
    this.props.history.push({
      pathname: '/Index/Details',
      state: {
        detail: item,
      },
    });
  }
  showModal = () => {
    this.setState({
      visible: true,
    });
  };

  handleOk = (e) => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };

  handleCancel = (e) => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };
  render() {
    let { readingdata } = this.props;
    return (
      <div>
        <p>推荐阅读</p>
        <div className={message.messagejuzhong}>
          <div>
            {readingdata && readingdata.length ? (
              readingdata.map((item, value) => {
                return (
                  <dl key={value} className={message.messagereading}>
                    <dd>
                      <div
                        className={message.a}
                        onClick={this.details.bind(this, item)}
                      >
                        <div className={message.dddiv1}>
                          <div className={message.biaotidivdis}>
                            <h3>{item.title}</h3>
                            <span>大约{time(item.updateAt)}</span>
                            {/* <span>{item.category.label}</span> */}
                          </div>
                          <p>{item.summary}</p>
                        </div>
                      </div>
                      <div className={message.dddiv2}>
                        <p>
                          <HeartOutlined style={{ fontSize: '15px' }} />
                          <span>{item.likes}</span>
                        </p>
                        <p>
                          <EyeOutlined style={{ fontSize: '15px' }} />
                          <span>{item.views}</span>
                        </p>
                        <p className={message.ppingheng}>
                          <ShareAltOutlined style={{ fontSize: '15px' }} />
                          <span>
                            <Button
                              type="primary"
                              onClick={this.showModal}
                              style={{ all: 'initial', cursor: 'pointer' }}
                            >
                              分享
                            </Button>
                          </span>
                        </p>
                      </div>
                    </dd>
                    <dt>
                      <img src={item.cover} alt="" />
                    </dt>
                  </dl>
                );
              })
            ) : (
              <div>暂无数据</div>
            )}
          </div>
        </div>
        <Modal
          title="Basic Modal"
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
          <QRCodeSVG value="https://reactjs.org/" />,<p>分享一下</p>
        </Modal>
      </div>
    );
  }
}

export default withRouter(Reading);
