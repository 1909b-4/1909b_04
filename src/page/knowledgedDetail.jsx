import React, { Component } from 'react';
import '../styles/knowledged.less';
import axios from 'axios';

export class Detail extends Component {
  state = {
    item: [],
    list: [],
  };

  componentDidMount() {
    this.setState({
      item: this.props.location.state,
    });
    axios.get('/api/article').then((res) => {
      this.setState({
        list: res.data.data[0],
      });
    });
  }

  // handleDetail(item) {
  //     console.log(111);
  //     this.props.history.push('/knowledgedDetail', item);
  // }

  render() {
    let { item, list } = this.state;
    function times(utc_datetime) {
      var T_pos = utc_datetime.indexOf('T');
      var Z_pos = utc_datetime.indexOf('Z');
      var year_month_day = utc_datetime.substr(0, T_pos);
      var hour_minute_second = utc_datetime.substr(
        T_pos + 1,
        Z_pos - T_pos - 1
      );
      var new_datetime = year_month_day + ' ' + hour_minute_second; // 2017-03-31 08:02:06
      timestamp = new Date(Date.parse(new_datetime));
      timestamp = timestamp.getTime();
      var timestamp = timestamp + 8 * 60 * 60 * 1000;
      var mistiming = Math.round((Date.now() - timestamp) / 1000);
      var arrr = ['年', '个月', '周', '天', '小时', '分钟', '秒'];
      var arrn = [31536000, 2592000, 604800, 86400, 3600, 60, 1];
      for (var i = 0; i < arrn.length; i++) {
        var inm = Math.floor(mistiming / arrn[i]);
        if (inm != 0) {
          return inm + arrr[i] + '前';
        }
      }
    }
    return (
      <div className="Detail">
        <div className="Detailmain">
          <div className="leftbox">
            <div className="webbox">
              <p>知识小册 / {item.title}</p>
              <h4>{item.title}</h4>
            </div>
            <div className="leftbox2">
              <div className="imgs">
                <img src={item.cover} alt="" />
              </div>
              <div className="header">
                <h3>{item.title}</h3>
                <p>{item.summary}</p>
                <p className="reade">
                  {item.views} 次阅读 · {item.createAt}
                </p>
                <button className="begin">开始阅读</button>
              </div>
            </div>
          </div>
          <div className="rightbox1">
            <div className="righttopbox1">
              <h3>其他知识笔记</h3>
            </div>
            <div className="rest1">
              {list && list.length > 0 ? (
                list.map((item, index) => {
                  // onClick={this.handleDetail.bind(this, item)}
                  return (
                    <div className="item1" key={index}>
                      <p className="titles1">
                        <span>{item.title}</span>{' '}
                        <span className="month">{times(item.updateAt)}</span>
                      </p>
                      <div className="box1">
                        <div className="title1">
                          <p className="summary1">{item.summary}</p>
                          <p className="content1">
                            <svg
                              viewBox="64 64 896 896"
                              focusable="false"
                              data-icon="eye"
                              width="1em"
                              height="1em"
                              fill="currentColor"
                              aria-hidden="true"
                            >
                              <path d="M942.2 486.2C847.4 286.5 704.1 186 512 186c-192.2 0-335.4 100.5-430.2 300.3a60.3 60.3 0 000 51.5C176.6 737.5 319.9 838 512 838c192.2 0 335.4-100.5 430.2-300.3 7.7-16.2 7.7-35 0-51.5zM512 766c-161.3 0-279.4-81.8-362.7-254C232.6 339.8 350.7 258 512 258c161.3 0 279.4 81.8 362.7 254C791.5 684.2 673.4 766 512 766zm-4-430c-97.2 0-176 78.8-176 176s78.8 176 176 176 176-78.8 176-176-78.8-176-176-176zm0 288c-61.9 0-112-50.1-112-112s50.1-112 112-112 112 50.1 112 112-50.1 112-112 112z"></path>
                            </svg>
                            <span>{item.views}</span>
                            <svg
                              viewBox="64 64 896 896"
                              focusable="false"
                              data-icon="share-alt"
                              width="1em"
                              height="1em"
                              fill="currentColor"
                              aria-hidden="true"
                            >
                              <path d="M752 664c-28.5 0-54.8 10-75.4 26.7L469.4 540.8a160.68 160.68 0 000-57.6l207.2-149.9C697.2 350 723.5 360 752 360c66.2 0 120-53.8 120-120s-53.8-120-120-120-120 53.8-120 120c0 11.6 1.6 22.7 4.7 33.3L439.9 415.8C410.7 377.1 364.3 352 312 352c-88.4 0-160 71.6-160 160s71.6 160 160 160c52.3 0 98.7-25.1 127.9-63.8l196.8 142.5c-3.1 10.6-4.7 21.8-4.7 33.3 0 66.2 53.8 120 120 120s120-53.8 120-120-53.8-120-120-120zm0-476c28.7 0 52 23.3 52 52s-23.3 52-52 52-52-23.3-52-52 23.3-52 52-52zM312 600c-48.5 0-88-39.5-88-88s39.5-88 88-88 88 39.5 88 88-39.5 88-88 88zm440 236c-28.7 0-52-23.3-52-52s23.3-52 52-52 52 23.3 52 52-23.3 52-52 52z"></path>
                            </svg>
                            <span>分享</span>
                          </p>
                        </div>
                        <div className="img1">
                          <img src={item.cover} alt="" />
                        </div>
                      </div>
                    </div>
                  );
                })
              ) : (
                <div>暂无数据</div>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Detail;
