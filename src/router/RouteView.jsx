import React, { Component, Suspense } from 'react';
import { Switch, Redirect, Route } from 'react-router-dom';
export class RouteView extends Component {
  render() {
    let { router = [] } = this.props;
    return (
      <Suspense fallback={'路由加载中'}>
        <Switch>
          {router.map((item, index) => {
            return (
              <Route
                path={item.path}
                key={index}
                render={(props) => {
                  return item.redirect ? (
                    <Redirect to={item.redirect}></Redirect>
                  ) : (
                    <item.component
                      {...props}
                      router={item.children}
                    ></item.component>
                  );
                }}
              ></Route>
            );
          })}
        </Switch>
      </Suspense>
    );
  }
}

export default RouteView;
