import { lazy } from 'react';

const router = [
  {
    path: '/Index',
    component: lazy(() => import('../page/Index')),
    children: [
      {
        path: '/Index/Home',
        component: lazy(() => import('../page/index/Home')),
      },
      {
        path: '/Index/File',
        component: lazy(() => import('../page/index/File')),
      },
      {
        path: '/Index/Property',
        component: lazy(() => import('../page/index/Property')),
      },
      {
        path: '/Index/Message',
        component: lazy(() => import('../page/index/Message')),
      },
      {
        path: '/Index/About',
        component: lazy(() => import('../page/index/About')),
      },
      {
        path: '/Index/article',
        component: lazy(() => import('../page/home/Article')),
      },
      {
        path: '/Index/category/fe',
        component: lazy(() => import('../page/home/category/Fe')),
      },
      {
        path: '/Index/category/be',
        component: lazy(() => import('../page/home/category/Be')),
      },
      {
        path: '/Index/category/reading',
        component: lazy(() => import('../page/home/category/Reading')),
      },
      {
        path: '/Index/category/linux',
        component: lazy(() => import('../page/home/category/Linux')),
      },
      {
        path: '/Index/category/leetcode',
        component: lazy(() => import('../page/home/category/Leetcode')),
      },
      {
        path: '/Index/category/news',
        component: lazy(() => import('../page/home/category/News')),
      },
      {
        path: '/Index/Details',
        component: lazy(() => import('../page/index/message/Details')),
      },
      {
        path: '*',
        component: lazy(() => import('../page/NoFind/NoFind')),
      },
      {
        path: '/',
        redirect: '/Index/Home',
      },
    ],
  },

  {
    path: '/knowledgedDetail',
    component: lazy(() => import('../page/knowledgedDetail')),
  },
  {
    path: '/',
    redirect: '/Index/Home',
  },
];
export default router;
