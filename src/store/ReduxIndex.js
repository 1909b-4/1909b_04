import { legacy_createStore, combineReducers, applyMiddleware } from 'redux';
import logger from 'redux-logger';
import thunk from 'redux-thunk';

import messageredux from './modules/messageredux';

let reducer = combineReducers({
  messageredux,
});

export default legacy_createStore(reducer, applyMiddleware(logger, thunk));
