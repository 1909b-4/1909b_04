const messlist = {
  boarddata: [],
  liuyanshangchuan: {
    children: [],
    content: '1',
    createAt: '',
    email: '',
    hostId: '',
    html: `<p></p>\n`,
    name: '小废物',
    parentCommentId: '',
    pass: '',
    replyUserEmail: '',
    replyUserName: '',
    updateAt: '',
    url: '',
    id: '568465465',
    userAgent: '',
  },
  huifuliuyan: {
    content: '1',
    createAt: '',
    email: '',
    hostId: '',
    html: `<p></p>\n`,
    name: '小废物',
    parentCommentId: '',
    pass: '',
    replyUserEmail: '',
    replyUserName: '',
    updateAt: '',
    url: '',
    id: '568465465',
    userAgent: '',
  },
  zongshu: 0,
  tiaoshu: 1,
  fenye: [],
};

let messageredux = (state = messlist, { type, payload }) => {
  switch (type) {
    case 'MESS_LIST':
      state.boarddata = payload.data[0];
      state.zongshu = state.boarddata.length + 1;
      state.fenye = state.boarddata.slice(
        (state.tiaoshu - 1) * state.zongshu,
        state.tiaoshu * state.zongshu
      );
      return {
        ...state,
        boarddata: [...state.boarddata],
        zongshu: state.zongshu,
        fenye: [...state.fenye],
      };
    case 'LIUYAN':
      state.liuyanshangchuan = {
        children: [],
        content: payload,
        createAt: '',
        email: '',
        hostId: '',
        html: `<p>${payload}</p>\n`,
        name: '小废物',
        parentCommentId: '',
        pass: '',
        replyUserEmail: '',
        replyUserName: '',
        updateAt: '',
        url: '',
        id: '568465465',
        userAgent: '',
      };
      state.boarddata.unshift(state.liuyanshangchuan);
      state.fenye = state.boarddata.slice(
        (state.tiaoshu - 1) * state.zongshu,
        state.tiaoshu * state.zongshu
      );
      return {
        ...state,
        boarddata: [...state.boarddata],
        liuyanshangchuan: { ...state.liuyanshangchuan },
        zongshu: state.zongshu,
        fenye: [...state.fenye],
      };
    case 'YI_HUIFU':
      state.huifuliuyan = {
        content: payload.item,
        createAt: '',
        email: '',
        hostId: '',
        html: `<p>${payload.item}</p>\n`,
        name: '小废物',
        parentCommentId: '',
        pass: '',
        replyUserEmail: '',
        replyUserName: state.boarddata[payload.inde].name,
        updateAt: '',
        url: '',
        id: '568465465',
        userAgent: '',
      };
      state.boarddata[payload.inde].children.unshift(state.huifuliuyan);
      return {
        ...state,
        boarddata: [...state.boarddata],
        huifuliuyan: { ...state.huifuliuyan },
      };
    case 'ER_HUIFU':
      state.huifuliuyan = {
        content: payload.item,
        createAt: '',
        email: '',
        hostId: '',
        html: `<p>${payload.item}</p>\n`,
        name: '小废物',
        parentCommentId: '',
        pass: '',
        replyUserEmail: '',
        replyUserName:
          state.boarddata[payload.inde.value].children[payload.inde.k].name,
        updateAt: '',
        url: '',
        id: '568465465',
        userAgent: '',
      };
      state.boarddata[payload.inde.value].children.push(state.huifuliuyan);
      return {
        ...state,
      };
    case 'TIAO_SHU':
      state.tiaoshu = payload;
      state.fenye = state.boarddata.slice(
        (state.tiaoshu - 1) * state.zongshu,
        state.tiaoshu * state.zongshu
      );
      return {
        ...state,
        tiaoshu: state.tiaoshu,
        fenye: [...state.fenye],
      };
    default:
      return state;
  }
};

export default messageredux;
