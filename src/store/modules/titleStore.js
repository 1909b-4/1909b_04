import { makeAutoObservable } from 'mobx';
import zhCN from '../../i18n/zh-CN';
import enUS from '../../i18n/en-US';

//定一个语言配置的对象
const messageLange = {
  'zh-CN': zhCN,
  'en-US': enUS,
};

console.log(messageLange, 'messageLange');
class titleStore {
  lange = navigator.language; //设置默认语言
  message = messageLange[this.lange]; // 设置默认语言配置 要么放中文的配置对象，要么英文的配置对象
  constructor() {
    makeAutoObservable(this);
  }

  changeLange() {
    //更改语言和语言配置
    this.lange = this.lange == 'zh-CN' ? 'en-US' : 'zh-CN';
    this.message = messageLange[this.lange];
  }
}

export default new titleStore();
